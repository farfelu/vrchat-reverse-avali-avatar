# VRChat Reverse Avali Avatar
### Simple project of a semi Quest compatible Reverse Avali Avatar for VRChat with performance rank PC ![good rank icon](.pages/rank-good.png) good and Quest ![medium rank icon](.pages/rank-medium.png) medium
This is a modified Reverse Avali with the base model from VictonRoy#4759/#2000 on the [Avali Army Discord](https://discord.gg/avali)  
It's fully set up with expressions, emotes and PhysBones

### Stats
|           | PC ![good rank icon](.pages/rank-good.png) | Quest ![medium rank icon](.pages/rank-medium.png) |
|-----------|-------|--------|
| Polygons  | 23134 | 15000  |
| Materials | 2     | 2      |

### Changes from the original
* Optimized UV Map
* Reduced material count to two
* Optional Quest compatible version
* MMD Blendshapes
* Avatar 3 Expressions

![Reverse Avali Avatar Preview](.pages/avatar-preview.jpg)

You can find a public demo version in the [**Reverse Avali Avatar Hall**](https://vrchat.com/home/launch?worldId=wrld_20ff4251-004e-4086-935f-1d1490719001) world in VRChat  
[![Reverse Avali Avatar Hall World thumbnail](.pages/reverse-avali-avatar-hall.jpg)](https://vrchat.com/home/launch?worldId=wrld_20ff4251-004e-4086-935f-1d1490719001)

https://vrchat.com/home/launch?worldId=wrld_20ff4251-004e-4086-935f-1d1490719001
## Requirements
* Unity 2019.4.31f1
  * check the VRChat documentation for the current supported version
  * https://docs.vrchat.com/docs/current-unity-version
* Poiyomi Shaders 7.2 or newer
  * https://github.com/poiyomi/PoiyomiToonShader
* VRChat SDK3 for Avatars Version **2022.1.2** or newer
  * https://vrchat.com/home/download

#### Optional
* Blender 3.0 or newer
* Substance Painter 7.2.3 or newer - alternatively a Photoshop PSD compatible program
  * https://www.adobe.com/products/substance3d-painter.html

## How to use
1. Clone or download this repository
2. Open the project in Unity (do not open any scene yet)
3. Import the VRChat SDK
4. Import Poiyomi Shaders
5. **Restart Unity and reload the project**
    * This is important, otherwise some behaviors on controllers fail to load correctly
6. Open *[Assets] -> [Reverse Avali] -> [Avali Scene]*  
or *[Assets] -> [Reverse Avali] -> [Quest Version] -> [Quest Avali Scene]* for the Quest version
7. Upload using the VRChat SDK  
https://docs.vrchat.com/docs/creating-your-first-avatar

## Customize
In *[Assets] -> [Reverse Avali] -> [Resources~]* (folder does not show up in Unity, use your file browser instead) are
* Blender (Version 3.0.0 or higher) .blend files
* Substance Painter files
  * Colors can be changed by changing the layer fill color
* untested Photoshop files exported from Substance Painter

## Quest limitations
The Quest does not support transparency, feathers will show with a solid background instead  
Simply add a background color instead of using transparency on the feather textures, otherwise artifacts will show on the mobile shader

![Quest feathers preview](.pages/quest-feathers.jpg)

## Write Defaults
This model is set up with Write Defaults **Off** based on [VRChats recommendations](https://docs.vrchat.com/docs/avatars-30#write-defaults-on-states)  
Animations are reset with reset animations  
Make sure to not mix Write Defaults On/Off Animation States

## Known issues
* The Quest feather texture needs a non transparent background, otherwise artifacts show on the transparent parts
* The FX Layer Controller has an empty mask to prevent the Gesture Layer Controller breaking, this is a workaround for a bug
https://feedback.vrchat.com/avatar-30/p/1017-bug-default-fx-layer-in-client-breaks-gesture-layer-animations-sample-insid
* The right arm puppet is implemented in the action layer instead of gesture layer as a workaround for a bug
https://feedback.vrchat.com/sdk-bug-reports/p/gesture-layers-will-motorcycle-pose-any-masked-humanoid-parts-that-are-not-curre
